import { Controller } from "react-hook-form";

interface Iprops {
  form: any;
  name: string;
  value: string;
  id: string;
}

function InputRadio(props: Iprops) {
  const { form, name, value, id } = props;

  return (
    <div className="pr-3 pl-3">
      <label htmlFor={id}>{value}</label>
      <Controller
        name={name}
        control={form.control}
        render={({ field: { onChange } }) => (
          <input
            type="radio"
            name={name}
            value={value}
            id={id}
            onChange={onChange}
          />
        )}
      />
    </div>
  );
}

export default InputRadio;
