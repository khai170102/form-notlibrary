import { Controller } from "react-hook-form";

interface Iprops {
  options: { label: string; value: string }[];
  form: any;
  name: string;
  label: string;
}

function InputSelect(props: Iprops) {
  const { options, form, name, label } = props;

  return (
    <div className="flex align-center">
      <Controller
        name={name}
        control={form.control}
        render={({ field: { onChange } }) => (
          <select onChange={onChange}>
            {options.map((option) => (
              <option value={option.value} key={option.value}>
                {option.label}{" "}
              </option>
            ))}
          </select>
        )}
      />
    </div>
  );
}

export default InputSelect;
