import { Controller } from "react-hook-form";
interface iProps {
  name: string;
  form: any;
}

function InputCheckbox(props: iProps) {
  const { name, form } = props;

  return (
    <div>
      <Controller
        name={name}
        control={form.control}
        render={({ field: { onChange } }) => (
          <input type="checkbox" name={name} onChange={onChange} />
        )}
      />
    </div>
  );
}

export default InputCheckbox;
