import { Controller } from "react-hook-form";

interface Iprops {
  name: string;
  type: string;
  form: any;
}
function InputField(props: Iprops) {
  const { name, type, form } = props;
  return (
    <Controller
      name={name}
      control={form.control}
      render={({ field: { onChange, value } }) => (
        <input name={name} type={type} onChange={onChange} />
      )}
    />
  );
}

export default InputField;
