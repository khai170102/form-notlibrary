import { useForm } from "react-hook-form";
import InputField from "./components/InputField";
import InputPassword from "./components/InputPassword";
import InputRadio from "./components/InputRadio";
import InputSelect from "./components/InputSelect";
import schemaRegister from "./components/Validation/RegisterValidation";
import { yupResolver } from "@hookform/resolvers/yup";
import InputCheckbox from "./components/InputCheckbox";

interface iProps {
  handleSubmit: Function;
}

const options = [
  {
    label: "foodball",
    value: "foodball",
  },
  {
    label: "watch movie",
    value: "watch movie",
  },
  {
    label: "gym",
    value: "gym",
  },
  {
    label: "skin care",
    value: "skin care",
  },
  {
    label: "custom keyboard",
    value: "custom keyboard",
  },
  {
    label: "sleep",
    value: "sleep",
  },
];

function FormRegister(props: iProps) {
  const form = useForm({
    defaultValues: {
      fullName: "",
      email: "",
      phoneNumber: "",
      gender: "",
      term: false,
      password: "",
      retypePassword: "",
      hobbies: "",
    },
    resolver: yupResolver(schemaRegister),
  });
  const { handleSubmit } = props;

  const onSubmit = (data: any) => {
    if (!handleSubmit) {
      return;
    }
    handleSubmit(data);
  };
  return (
    <form
      onSubmit={form.handleSubmit(onSubmit)}
      className="shadow-2xl bg-slate-800 p-4 rounded-md"
    >
      <label>Fullname</label>
      <InputField name="fullName" type="name" form={form} />
      <label className="text-red-600">
        {form.formState.errors.fullName?.message}
      </label>

      <label>Email</label>
      <InputField name="email" type="email" form={form} />
      <label className="text-red-600">
        {form.formState.errors.email?.message}
      </label>

      <label>Phone number</label>
      <InputField name="phoneNumber" type="phone" form={form} />
      <label className="text-red-600">
        {form.formState.errors.phoneNumber?.message}
      </label>

      <div className="flex justify-between">
        <div>
          <div className="flex justify-around">
            <label>Gender : </label>
            <InputRadio name="gender" value="man" id="gender-man" form={form} />
            <InputRadio
              name="gender"
              value="wommen"
              id="gender-wommen"
              form={form}
            />
          </div>
          <label className="text-red-600">
            {form.formState.errors.gender?.message}
          </label>
        </div>

        <div>
          <label>Hobbies : </label>
          <InputSelect
            name="hobbies"
            label="hobbies"
            form={form}
            options={options}
          />
          <label className="text-red-600">
            {form.formState.errors.hobbies?.message}
          </label>
        </div>
      </div>

      <label>Password</label>
      <InputPassword name="password" type="password" form={form} />
      <label className="text-red-600">
        {form.formState.errors.password?.message}
      </label>

      {/* {errors.password && <p>{errors.password.message}</p>} */}

      <label>Retype Password </label>
      <InputPassword name="retypePassword" type="password" form={form} />
      <label className="text-red-600">
        {form.formState.errors.retypePassword?.message}
      </label>

      <div className="flex items-center	 ">
        <div className="pr-3 pt-3">
          <InputCheckbox name="term" form={form} />
        </div>
        <label>Confirm</label>
      </div>
      <label className="text-red-600">
        {form.formState.errors.term?.message}
      </label>

      <input type="submit" />
    </form>
  );
}

export default FormRegister;
