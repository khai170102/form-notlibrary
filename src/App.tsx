import FormRegister from "./FormRegister";
import "./style.css";

function App() {
  const handleSubmit = (value: any) => {
    alert(
      "full name : " +
        value.fullName +
        " email : " +
        value.email +
        " phone number : " +
        value.phoneNumber +
        " gender : " +
        value.gender +
        " hobbies  : " +
        value.hobbies +
        " password : " +
        value.password +
        " retypePassword : " +
        value.retypePassword
    );
  };
  return (
    <div>
      <div>
        <FormRegister handleSubmit={handleSubmit} />
      </div>
    </div>
  );
}

export default App;
